const mongoose = require("mongoose");
const {DB} = require("./config")
let dbURL = "";

dbURL = DB.PROTOCOL+"://"+DB.HOST+":"+DB.PORT+"/"+DB.NAME;

mongoose.connect(dbURL,{
    autoCreate: true,
    autoIndex: true,
}); 