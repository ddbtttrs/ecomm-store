const ProductModel = require("../models/product.model");
const OrderModel = require("../models/order.model");
const { created_by } = require("../models/common.schema");

class OrderController{
    createOrder = async (req, res, next) => {
        try{
            let payload = req.body;
            
            let order_data =  {
                buyer_id: req.auth_user._id,
                cart: [],
                sub_total: 0,
                discount: {
                    ...payload.discount
                },
                service_charge: payload.service_charge ? payload.service_charge : 0,
                dilivery_charge: payload.dilivery_charge ? payload.dilivery_charge : 0,
                tax:0,
                total_amount: 0, // this has to be updated
                is_featured: false,
                order_date: Date.now(),
                is_paid: {
                    ...payload.is_paid
                },
                created_by: req.auth_user._id,
                status: "pending",
            };
            let cart = [];
            let sub_total = 0;

            let cart_payload_ids = payload.cart.map((item) => item.product_id);
            let cart_product = await ProductModel.find({
                _id: {
                    $in: cart_payload_ids
                }
            });           


            cart_product.map((product) => {               
               let curr_qty = 0;
               payload.cart.map((item) => {
                if(item.product_id == product._id) {
                    curr_qty = item.qty
                } else (console.log("error!!"));                
               })
               let item_total = curr_qty * product.actual_price;
                let single_item = {
                    product_id: product._id,
                    qty: Number(curr_qty),
                    total_amt: item_total
                };
                
                cart.push(single_item);
                sub_total += item_total;
            })

            order_data.cart = cart
            order_data.sub_total = sub_total
            let discaount_amt = 0;
            if(order_data.discount) {
                if (order_data.discount.discount_type == "percent") {
                discaount_amt = sub_total * Number(order_data.discount.amount) *1/100 ;
            } else {
                discaount_amt = Number(order_data.discount.amount);
            };
            };
            order_data.total_amount = Math.ceil (sub_total - discaount_amt + Number(order_data.service_charge)+ Number(order_data.dilivery_charge) + Number(order_data.tax))
            let order = new OrderModel(order_data);
            await order.save() 
            if(order){
            res.json({
                resposnse: order,
                status: true,
                msg: "Your Order has been placed Sucessfully" 
            });
            } else {
                next({status: 400, msg: order});
            }

        }catch(except){
            console.log("Order Controller: ", except);
            next({status: 400, msg: except});
        };
    }
};

module.exports = OrderController;