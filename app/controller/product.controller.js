const slugify = require("slugify");
const ProductService = require("../services/product.service");

class ProductController {
    constructor () {
        this.product_srvc = new ProductService
    }
    productStore = async (req,res,next) =>{
        try{
            let data = req.body;
            data.images = [];
            if(req.files){
                req.files.map((item)=>{
                    data.images.push(item.filename)
                });
            };
            data.slug = slugify(data.name, {
                lower: true
            });


            if(!data.brand || data.brand == "null"){
                data.brand = null;
            };

            if(!data.category_id || data.category_id == "null"){
                data.category_id = null;
            } else {
                data.category_id = data.category_id.split(",");
            };

            if(!data.seller || data.seller == "null"){
                data.seller = null;
            };

            data.actual_price = data.price - data.price*data.discount/100;
            data.is_featured = data.is_featured ? true : false;

            this.product_srvc.storeValiate(data, req.auth_user._id);

            let response = await this.product_srvc.createProduct()
            res.json({
                result: response,
                msg: "Product Created Sucessfully",
                status: true
            });

        } catch(except){
            console.log("ProductStore: "+ except);
            next({status: 400, msg: except});
        }
    }

    getProduct = async (req, res, next) => {
        try{
            console.log(req.params)
            let paginate = {
                total_count: await this.product_srvc.getAllCount(),
                per_page: req.query.per_page ? parseInt(req.query.per_page) : 10,
                current_page: req.query.page ? parseInt(req.query.page) : 1
            }
            let skip = (paginate.current_page -1)* paginate.per_page;
            let data = await this.product_srvc.getProducts(skip, paginate.per_page);

            res.json({
                result: data,
                status: true,
                paginate: paginate,
                msg: "Data fetched"
            })

        } catch (except) {
            console.log("getProduct: ", except);
            next({status: 400, msg: except});
        };
    }

    getProductById = async (req, res, next) => {
        try{
            
            let data = await this.product_srvc.productById(req.params.id)
            if (data){
                res.json({
                    result: data,
                    status: true,
                    msg: "Data Fetched"
                });

                } else {
                    next({status: 404, msg: "resource not found"});
                };

            } catch (except) {
            console.log("getProductById: ", except)
            next({status: 404, msg: except})
        }
    }

    deleteProductById = async (req, res, next) => {
        try{
            
            let data = await this.product_srvc.deleteById(req.params.id)
            if (data){
                res.json({
                    result: data,
                    status: true,
                    msg: "Data Deleted"
                });
                
                } else {
                    next({status: 404, msg: "resource not found"});
                };

            } catch (except) {
            console.log("getProductById: ", except)
            next({status: 404, msg: except})
        }
    }

    updateStore = async (req,res,next) =>{
        try{
            let current_data = await this.product_srvc.productById(req.params.id);
            
            let data = req.body;
            data.images = current_data.images;
            if(req.files){
                req.files.map((items) => {
                    data.images.push(items.filename)
                });
            }
            data.slug = current_data.slug;
            
            if(!data.brand || data.brand == "null"){
                data.brand = null;
            };

            if(!data.category_id || data.category_id == "null"){
                data.category_id = null;
            } else {
                data.category_id = data.category_id.split(",");
            };

            if(!data.seller || data.seller == "null"){
                data.seller = null;
            };

            data.actual_price = data.price - data.price*data.discount/100;
            data.is_featured = data.is_featured ? true : false;
            
            this.product_srvc.storeValiate(data);
            let response = await this.product_srvc.updateProduct(req.params.id)
            res.json({
                result: response,
                msg: "Product Updated Sucessfully",
                status: true
            });

        } catch(except){
            console.log("UpdateStore: "+ except);
            next({status: 400, msg: except});
        }
    }

};

module.exports = ProductController;