


const UserService = require("../services/user.service");
const validateUser = new UserService();
const nodemailer = require("nodemailer");
const {MongoClient} = require("mongodb");
const bcrypt = require('bcrypt');
const jwt  = require("jsonwebtoken");
const Config = require("../../config/config");
class AuthController{
    constructor () {
        this.user_svc = new UserService()
    };

    registerUser = async (req, res, next) => {
      
        // Data entry ===> User where? ===> Form
        // Submission of the form, where? ===> /register route of BE
         try{           
            let data = req.body;
            if(req.file){
            data.image = req.file.filename            
            }
            this.user_svc.validateUser(data);
            data.password = bcrypt.hashSync(data.password, 10)
            // // Database operation 
            // let client = await MongoClient.connect("mongodb://localhost:27017");
            // let db = client.db("stack-15");

            // // data insert
            // let user_data = await db.collection("users").insertOne(data);

            let db_data = await this.user_svc.createUser(data);
        

            // mail send code
            // const transporter = nodemailer.createTransport({
            //     host: 'smtp.ethereal.email',
            //     port: 587,
            //     auth: {
            //         user: 'vincenza.nicolas56@ethereal.email',
            //         pass: '1E711EBtQFsuBmcjDh'
            //     }
            // });
            // console.log("Im here");

            // const respond_email = await transporter.sendMail({
            //     from: "noreply@test.com",
            //     to: "bistasarthak@gmail.com",
            //     subject: "hello you have been auhorised",
            //     text: "hello  you email has been verified.",
            // });

          

            res.json({
                result: db_data,
                status: true,
                msg: "Register data test"
            });
        

        } catch (excp){
            console.log(excp)
            next({
                status: 400,
                mesg: excp             
            });
        };    
     
    }

    loginUser = async (req, res, next) => {
      try{
        let data = req.body;
        let loggedInUser = await this.user_svc.getUsrByEmail(data);
        if(loggedInUser) {
            if(bcrypt.compareSync(data.password, loggedInUser.password)) {
                let token = jwt.sign({
                    user_id: loggedInUser._id
                }, Config.JWT_SECRET)
                res.json({
                    result: {
                        user: loggedInUser,
                        acess_token: token
                    },
                    status: true,
                    msg: "Logged in succesfully"
                })
            } else {
                next({status: 400, msg: "Password does not match"})
            }
        } else {
            next({status: 400, msg: "Credential does not match"})
        }

      } catch (excpt) {
        console.log("Login: ", excpt);
        next({status: 400, msg: excpt});
      }
    }

    getParam = (req, res, next) => {
        let params = req.params;
        let query = req.query;
        let body = req.body;
        res.json({
            results: {
                param: params,
                query: query,
                body: body
            },
            status: true,
            msg : "parser example"
        });
    }

    logout = (req, res, next) => {
 
    }

    getLoggedInUser = (req, res, next) => {
        res.json({
            result: req.auth_user,
            status: true,
            msg: "your Profile"
        })
    }
}

module.exports = AuthController;

