const slugify = require("slugify");
const CategoryService = require("../services/category.service");

class CategoryController {
    constructor () {
        this.category_srvc = new CategoryService
    }
    categoryStore = async (req,res,next) =>{
        try{
            let data = req.body;
            if(req.file){
                data.image = req.file.filename;
            };
            data.slug = slugify(data.name, {
                lower: true
            });

            if(!data.brand || data.brand == "null"){
                data.brand = null;
            };

            if(!data.parent_id || data.parent_id == "null"){
                data.parent_id = null;
            };
            console.log(req.auth_user._id)
            data.created_by = req.auth_user._id.toString();
            


            this.category_srvc.storeValiate(data);
            let response = await this.category_srvc.createCategory()
            res.json({
                result: response,
                msg: "Category Created Sucessfully",
                status: true
            });

        } catch(except){
            console.log("CategoryStore: "+ except);
            next({status: 400, msg: except});
        }
    }

    getCategory = async (req, res, next) => {
        try{
            console.log(req.params)
            let paginate = {
                total_count: await this.category_srvc.getAllCount(),
                per_page: req.query.per_page ? parseInt(req.query.per_page) : 10,
                current_page: req.query.page ? parseInt(req.query.page) : 1
            }
            let skip = (paginate.current_page -1)* paginate.per_page;
            let data = await this.category_srvc.getCategories(skip, paginate.per_page);

            res.json({
                result: data,
                status: true,
                paginate: paginate,
                msg: "Data fetched"
            })

        } catch (except) {
            console.log("getCategory: ", except);
            next({status: 400, msg: except});
        };
    }

    getCategoryById = async (req, res, next) => {
        try{
            
            let data = await this.category_srvc.categoryById(req.params.id)
            if (data){
                res.json({
                    result: data,
                    status: true,
                    msg: "Data Fetched"
                });

                } else {
                    next({status: 404, msg: "resource not found"});
                };

            } catch (except) {
            console.log("getCategoryById: ", except)
            next({status: 404, msg: except})
        }
    }

    deleteCategoryById = async (req, res, next) => {
        try{
            
            let data = await this.category_srvc.deleteById(req.params.id)
            if (data){
                res.json({
                    result: data,
                    status: true,
                    msg: "Data Deleted"
                });
                
                } else {
                    next({status: 404, msg: "resource not found"});
                };

            } catch (except) {
            console.log("getCategoryById: ", except)
            next({status: 404, msg: except})
        }
    }

    updateStore = async (req,res,next) =>{
        try{
            let current_data = await this.category_srvc.categoryById(req.params.id);

            let data = req.body;
            if(req.file){
                data.image = req.file.filename;
            } else {
                data.image = current_data.image
            };
            data.slug = current_data.slug;
            
            if(!data.brand || data.brand == "null"){
                data.brand = null;
            };

            if(!data.parent_id || data.parent_id == "null"){
                data.parent_id = null;
            };
            this.category_srvc.storeValiate(data);
            let response = await this.category_srvc.updateCategory(req.params.id)
            res.json({
                result: response,
                msg: "Category Updated Sucessfully",
                status: true
            });

        } catch(except){
            console.log("UpdateStore: "+ except);
            next({status: 400, msg: except});
        }
    }

};

module.exports = CategoryController;