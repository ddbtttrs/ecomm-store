const slugify = require("slugify");
const LableService = require("../services/label.service");

class LabelController {
    constructor () {
        this.label_srvc = new LableService
    }
    labelStore = async (req,res,next) =>{
        try{
            let data = req.body;
            if(req.file){
                data.image = req.file.filename;
            };
            
            data.type = req.params.type;
            if(!data.link || data.link == "null"){
                data.link = slugify(data.title, {
                    lower: true
                });
            };

            this.label_srvc.storeValiate(data);
            let response = await this.label_srvc.createLabel()
            res.json({
                result: response,
                msg: data.type+" Created Sucessfully",
                status: true
            });

        } catch(except){
            console.log("LabelStore: "+ except);
            next({status: 400, msg: except});
        }
    }

    getLabel = async (req, res, next) => {
        try{
            console.log(req.params)
            let paginate = {
                total_count: await this.label_srvc.getAllCount(req.params.type),
                per_page: req.query.per_page ? parseInt(req.query.per_page) : 10,
                current_page: req.query.page ? parseInt(req.query.page) : 1
            }
            let skip = (paginate.current_page -1)* paginate.per_page;
            let data = await this.label_srvc.getlabel(req.params.type, skip, paginate.per_page);

            res.json({
                result: data,
                status: true,
                paginate: paginate,
                msg: "Data fetched"
            })

        } catch (except) {
            console.log("getLabel: ", except);
            next({status: 400, msg: except});
        };
    }

    getLabelById = async (req, res, next) => {
        try{
            
            let data = await this.label_srvc.labelById(req.params.type, req.params.id)
            if (data){
                res.json({
                    result: data,
                    status: true,
                    msg: "Data Fetched"
                });

                } else {
                    next({status: 404, msg: "resource not found"});
                };

            } catch (except) {
            console.log("getLableById: ", except)
            next({status: 404, msg: except})
        }
    }

    deleteLabelById = async (req, res, next) => {
        try{
            
            let data = await this.label_srvc.deleteById(req.params.type, req.params.id)
            if (data.deletedCount){
                res.json({
                    result: data,
                    status: true,
                    msg: "Data Deleted"
                });
                
                } else {
                    next({status: 404, msg: "resource not found"});
                };

            } catch (except) {
            console.log("getLableById: ", except)
            next({status: 404, msg: except})
        }
    }

    updateStore = async (req,res,next) =>{
        try{
            let current_data = await this.label_srvc.labelById(req.params.type, req.params.id);

            let data = req.body;
            if(req.file){
                data.image = req.file.filename;
            } else {
                data.image = current_data.image
            };
            
            data.type = req.params.type;
            if(!data.link || data.link == "null"){
                if (req.params.type === "banner") {
                    data.link = slugify(data.title, {
                        lower: true
                    });
                } else {
                    data.link = current_data.link
                };                
            };

            this.label_srvc.storeValiate(data);
            let response = await this.label_srvc.updateLabel(req.params.id)
            res.json({
                result: response,
                msg: data.type+" Updated Sucessfully",
                status: true
            });

        } catch(except){
            console.log("UpdateStore: "+ except);
            next({status: 400, msg: except});
        }
    }

};

module.exports = LabelController;