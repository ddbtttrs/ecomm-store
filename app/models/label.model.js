const mongoose = require("mongoose");
const commonSchema = require("./common.schema")
const LabelSchemaDef = new mongoose.Schema({
    title: {
        type: String,
        required: true
    },
    link: String,
    image: {
        type: String,
        required: true,
    },
    status: commonSchema.statusschema,
    created_by: commonSchema.created_by,
    type: {
        type: String,
        enum: ['banner','brand'],
        default: 'banner'
    },
}, commonSchema.trigger)

const LabelModel = mongoose.model("Label", LabelSchemaDef);
module.exports = LabelModel; 