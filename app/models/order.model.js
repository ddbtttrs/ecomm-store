const mongoose = require("mongoose");
const commonSchema = require("./common.schema");
const { required } = require("joi");
const OrderSchemaDef = new mongoose.Schema({
    buyer_id: {
        type: mongoose.Types.ObjectId,
        ref: "User",
        required: true,
    },
    cart: [{
        product_id: {
            type: mongoose.Types.ObjectId,
            ref: "Product",
            required: true
        },
        qty: {
            type: Number,
            reuired: true
        },
        total_amt: {
            type: Number,
            required: true
        },
    }],
    sub_total: {
        type: Number,
        required: true
    },
    dilivery_charge: Number,
    discount: {
        discount_type: {
        type: String,
        enum: ['percent', 'flat'],
        default: 'percent'
        },
        amount: Number
    },
    service_charge: Number,
    total_amount: Number,    
    tax: Number,    
    is_featured: {
        type: Boolean,
        default: false
    },
    order_date: Date,
    seller: {
        type: mongoose.Types.ObjectId,
        ref: "User",
        default: null
    },    
    status: {
        type: String,
        enum: ['pending','verified','processing','cancelled','delivered'],
        default: 'pending'
    },
    is_paid:{
        paid: {type: Boolean, default: false},
        transcation: {type: String, default: null}
    },
    created_by: commonSchema.created_by,    
   
}, commonSchema.trigger)

const OrderModel = mongoose.model("Order", OrderSchemaDef);
module.exports = OrderModel; 