const mongoose = require("mongoose");
const commonSchema = require("./common.schema");
const { required, ref } = require("joi");
const CategorySchemaDef = new mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    slug: {
        type: String,
        required: true        
    },
    image: String,
    parent_id: {
        type: mongoose.Types.ObjectId,
        ref: "Category",
        default: null 
    },
    brand: [{
        type: mongoose.Types.ObjectId,
        ref: "Label",
        default: null
    }],    
    status: commonSchema.statusschema,
    created_by: commonSchema.created_by,
   
}, commonSchema.trigger)

const CategoryModel = mongoose.model("Category", CategorySchemaDef);
module.exports = CategoryModel; 