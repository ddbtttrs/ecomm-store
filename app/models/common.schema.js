const mongoose = require("mongoose");

const trigger = {
    timestamps: true,
    autoCreate: true,
    autoIndex: true,
    timeseries: true
};

const created_by = {
    type: mongoose.Types.ObjectId,
    ref: "User",
    // default: null
};    

const statusschema = {
    type: String,
    enum: ['active', 'inactive'],
    default: 'active'
};

module.exports = {trigger,
    created_by,
    statusschema}

