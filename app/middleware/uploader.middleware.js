const multer = require('multer');
//const uploader = multer({ dest: 'uploads/'})

const storage = multer.diskStorage
({
    destination: (req, file, next) =>{
        next(null, "uploads/");
    },

    filename: (req, file, next) => {
    let file_name = Date.now() +"-"+file.originalname;
    next(null, file_name)
    }
});

imageFilter = (req, file, next) => 
{
    let allowed = ["apng", "avif", "gif", "jpeg", "png", "svg+xml", "webp"];
    let file_parts = file.originalname.split('.');
    let ext = file_parts.pop().toLowerCase();
    //console.log(ext);

    if(allowed.includes(ext)) {
        next(null, true);
    } else {
        next({status: 404, msg: "Image file format not suppored"}, null)
    };
        // for(let elms of allowed) 
    // {
    //     //console.log(elms)
    //     if(elms === ext) 
    //     {            
    //    console.log("I am here");
    //     };
    // };  

    //console.log(file);    
};

const upload = multer
({
    storage: storage,
    fileFilter: imageFilter,
    // limits: {
    //     fileSize: 5000000,
    // }
});

module.exports = upload; 