const Joi = require("joi");
// const DBService = require("./db.service");
// const { ObjectId } = require("mongodb");
const UserModel = require("../models/users.model") 
class UserService{
    validateUser = (data) => {
       try{
            let userSchema = Joi.object({
                name: Joi.string().min(3).required(),
                email: Joi.string().email().required(),
                password: Joi.string().min(8).required(),
                address: Joi.string(),
                role: Joi.string().required(),
                status: Joi.string().required(),
                image: Joi.string().empty()
                     
            })
        
            let response = userSchema.validate(data);
            if(response.error){
                throw response.error;
            }
       } catch(err) {
            console.log(err);
            throw err;
       }
    }

    createUser = async (data) => {
        try {
            let user_obj = new UserModel(data);
            
            return  await user_obj.save();
        } catch(excep){
            if(excep.code === 11000){
                let keys = Object.keys(excep.keyPattern);
                throw keys.join(', ')+" should be unique"
            } else {
            throw excep
            }
            throw excep 
        }

        
    }

    getUsrByEmail = async (data) => {
        try {
            //find in database userinput 
            // SELECT * FROM Users WHERE email = data.email AND password = data.password
            let result =  await UserModel.findOne({
                email: data.email
            });
            
            return result

        } catch(excpt) {
            throw excpt
        }
    }

    getUsrById = async (id) => {
        try{
            
            // let user = await this.db.collection('users').findOne({
            //     _id: new ObjectId(id)

            let user = await UserModel
                                .findById(id)
                                .populate("created_by")
        

            return user;

        } catch (err) {
            throw err; 
        }
    }
}

module.exports = UserService;