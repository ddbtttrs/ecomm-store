const joi = require("joi");
const CategoryModel = require("../models/category.model");
const { created_by } = require("../models/common.schema");

class CategoryService {
    storeValiate = (data) => {
        let schema = joi.object({
            name: joi.string().required().min(2),
            slug: joi.string(),
            image: joi.string(),
            parent_id: joi.string().allow(null, "").default(null),
            brand: joi.string().allow(null, "").default(null),
            created_by: joi.string().allow(null, ""),
            status: joi.string().valid("active", "incative").default("inactive")
        });
        let response = schema.validate(data);
        if(response.error){
            throw response.error.details[0].message
        } else {
            this.data = data;
            this.data.brand = data.brand.split(",");
        }        
    };   
    
    createCategory = async () => {
        let category_obj = new CategoryModel(this.data);
        return await category_obj.save();
    }

    getAllCount = async () =>{
        
        let all_data = CategoryModel.find();
        return all_data.length;
    };
    
    getCategories = async (skip, limit) => {
        return await CategoryModel.find()
                        .populate("parent_id")
                        .populate("brand")
                        .skip(skip)
                        .limit(limit);
    };

    // category_by_id = CategoryService.findModelById(CategoryModel);
    categoryById = async (id) => {
        return await CategoryModel.findById(id)
                        .populate("parent_id")
                        .populate("brand");
    }

    deleteById = async (id) => {
        return await CategoryModel.findByIdAndDelete(id)
    }

    updateCategory = async (id) => {
        let status = await CategoryModel.findByIdAndUpdate(id, {
            $set: this.data
        });
        return status;
    };
}

module.exports = CategoryService;