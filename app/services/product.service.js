const joi = require("joi");
const ProductModel = require("../models/product.model");
const { created_by } = require("../models/common.schema");
class ProductService {
    storeValiate = (data, created_by) => {
        let schema = joi.object({
            name: joi.string().required().min(2),
            slug: joi.string(),
            images: joi.array().items(joi.string().allow(null).allow("")),
            description: joi.string().allow(null, "").default(null),
            price: joi.number().required().min(1),
            actual_price: joi.number().required().min(1),
            discount: joi.number().allow(null, '').min(0).max(100),
            category_id: joi.array().items(joi.string().allow(null).allow("")),
            brand: joi.string().allow(null, "").default(null),
            seller: joi.string().allow(null, ""),
            is_featured: joi.boolean().default(false),
            status: joi.string().valid("active", "incative").default("inactive")
        });
        let response = schema.validate(data);
        if(response.error){
            throw response.error.details[0].message
        } else {

            this.data = data;
            this.data.created_by = created_by
            // this.data.brand = data.brand.split(",");
            console.log(data);
        }        
    };   
    
    createProduct = async () => {
        let product_obj = new ProductModel(this.data);
        return await product_obj
                        .save()
                        
    }

    getAllCount = async () =>{
        
        let all_data = ProductModel
                            .find()
                            .populate("created_by")
        return all_data.length;
    };
    
    getProducts = async (skip, limit) => {
        return await ProductModel.find()
                        .populate("category_id")
                        .populate("brand")
                        .populate("seller")
                        .populate("created_by")
                        .skip(skip)
                        .limit(limit);
    };

    productById = async (id) => {
        return await ProductModel.findById(id)
                        .populate("category_id")
                        .populate("brand")
                        .populate("seller")
                        .populate("created_by")
    }

    deleteById = async (id) => {
        return await ProductModel
                        .findByIdAndDelete(id)
                        .populate("created_by")
    }

    updateProduct = async (id) => {
        let status = await ProductModel
                            .findByIdAndUpdate(id, {
                                $set: this.data
                            })
                            .populate("created_by");
        return status;
    };
}

module.exports = ProductService;