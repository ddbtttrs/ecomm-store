const joi = require("joi");
const LabelModel = require("../models/label.model")
class LableService {
    storeValiate = (data) => {
        let schema = joi.object({
            title: joi.string().required().min(2),
            link: joi.string(),
            type: joi.string().valid('banner', 'brand'),
            image: joi.string(),
            status: joi.string().valid("active", "incative").default("inactive")
        });
        let response = schema.validate(data);
        if(response.error){
            throw response.error.details[0].message
        } else {
            this.data = data;
        }        
    };   
    
    createLabel = async () => {
        let label_obj = new LabelModel(this.data);
        return (await label_obj.save())
                            .populate("created_by");
    }

    getAllCount = async (type) =>{
        let filter = {
            type: type
        };
        let all_data = LabelModel
                        .find(filter)
                        .populate("created_by");;
        return all_data.length;
    };
    
    getlabel = async (type, skip, limit) => {
        let filter = {
            type: type
        };
        return await LabelModel.find(filter)
                        .skip(skip)
                        .limit(limit)
                        .populate("created_by");;
    };

    labelById = async (type, id) => {
        let filter = {
            type: type,
            _id: id
        };
        return await LabelModel
                        .findOne(filter)
                        .populate("created_by");
    }

    deleteById = async (type, id) => {
        let filter = {
            type: type,
            _id: id
        };
        return await LabelModel
                        .deleteOne(filter)
                        .populate("created_by");
    }

    updateLabel = async (id) => {
        let status = await LabelModel.findByIdAndUpdate(id, {
            $set: this.data
        });
        return status;
    };
}

module.exports = LableService;