const express = require("express");
const app = express.Router();
const auth = require('../app/middleware/auth.middleware')


const AuthController = require("../app/controller/auth.controller")
let auth_cntrl = new AuthController();

const upload = require("../app/middleware/uploader.middleware")



app.get("/user/:id", auth_cntrl.getParam);

app.post("/login", auth_cntrl.loginUser);

app.post("/register", upload.single('image'), auth_cntrl.registerUser) 

app.post("/logout", auth,  auth_cntrl.logout)

app.get("/me", auth, auth_cntrl.getLoggedInUser)
module.exports = app;  
