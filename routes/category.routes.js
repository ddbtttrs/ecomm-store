const auth = require("../app/middleware/auth.middleware");
const { isAdmin } = require("../app/middleware/rbac.middleware");
const upload = require("../app/middleware/uploader.middleware");
const CategoryController = require("../../api/app/controller/category.controller");

const category_cntrl = new CategoryController();

const router = require("express").Router();

router.route("/")
    .post(auth, isAdmin, upload.single('image'), category_cntrl.categoryStore)
    .get(category_cntrl.getCategory);

router.route("/:id")
    .get(category_cntrl.getCategoryById )
    .delete(auth, isAdmin, category_cntrl.deleteCategoryById)
    .put(auth, isAdmin, upload.single('image'), category_cntrl.updateStore)


module.exports = router;
