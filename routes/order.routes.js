const router = require("express").Router();
const auth = require("../app/middleware/auth.middleware");
const { isCustomer, isSeller } = require("../app/middleware/rbac.middleware");
const OrderController = require("../app/controller/order.controller");
const order_cntrl = new OrderController
router.post("/", auth, isCustomer, order_cntrl.createOrder)

module.exports = router


