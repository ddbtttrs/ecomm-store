const auth = require("../app/middleware/auth.middleware");
const { isAdmin } = require("../app/middleware/rbac.middleware");
const upload = require("../app/middleware/uploader.middleware");
const LabelController = require("../../api/app/controller/label.controller");

const label_cntrl = new LabelController();

const router = require("express").Router();
const validateType = (req,res,next) => {
    if(req.params.type === 'banner' || req.params.type === 'brand') {
        next();
    } else {
        next({ status: 404, msg: "resource not found"});
    }
}
router.route("/:type")
    .post(validateType, auth, isAdmin, upload.single('image'), label_cntrl.labelStore)
    .get(validateType, label_cntrl.getLabel);

router.route("/:type/:id")
    .get(validateType,label_cntrl.getLabelById )
    .delete(validateType, auth, isAdmin, label_cntrl.deleteLabelById)
    .put(validateType, auth, isAdmin, upload.single('image'), label_cntrl.updateStore)


module.exports = router;
