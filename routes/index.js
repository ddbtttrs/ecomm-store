//
const express = require("express");
const app = express();
const auth_routes = require("./auth.routes")
const cat_routes = require('./category.routes')
const label_routes = require("./label.routes");
const product_routes = require("./product.routes");
const order_routes = require("./order.routes");

app.get("/image", (req, res, next) => {
    res.render("image");
});

app.use(auth_routes);

app.use("/cat", cat_routes);

app.use("/product", product_routes);

app.use("/order", order_routes);
// register at last
app.use(label_routes);

// app.use("/", (req,res) => {

// });

module.exports = app;
