const auth = require("../app/middleware/auth.middleware");
const { isAdmin } = require("../app/middleware/rbac.middleware");
const upload = require("../app/middleware/uploader.middleware");
const ProductController = require("../../api/app/controller/product.controller");

const product_cntrl = new ProductController();

const router = require("express").Router();

router.route("/")
    .post(auth, isAdmin, upload.array('images'), product_cntrl.productStore)
    .get(product_cntrl.getProduct);

router.route("/:id")
    .get(product_cntrl.getProductById )
    .delete(auth, isAdmin, product_cntrl.deleteProductById)
    .put(auth, isAdmin, upload.array('images'), product_cntrl.updateStore)


module.exports = router;
