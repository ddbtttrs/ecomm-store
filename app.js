const express = require('express');
const app = express();
const routes = require("./routes/")
const router = express.Router();
const path = require("path");
require("../api/config/mongoose.config")


app.use("/assets", express.static("uploads/" ))
// Path setting
app.set("view engine", "pug");
app.set("views", path.join(__dirname, "/views"));
console.log(path.join(__dirname, "/views"));

// app.set("view engine", "ejs");
// app.set("views", process.cwd() + "/views");
// console.log(process.cwd() + "/views");

// using path.join ==> current WDIR
//path.join(__dirname, "/views");


// every URL must register in app
// router.get('/', (req, res) =>{
//     res.json({
//         status: true,
//         message: "I am at Router",
//     });
// });

/**
 * a. Application Level Middleware
 * b. Routiing Level Middleware
 * c. Static Middelware
 * d. Built in Middleware / Third party Middleware
 * e. Custom Middleware
 * f. Error Handling Middleware 
 */

// Body Parser
app.use(express.json());
app.use(express.urlencoded({
    extended: false
}));

/** routing of muonts */
app.use(routes);
app.use(router);

// 404 Handeling
// app.use((req,res, next) => {
//     next({status: 404, msg: "Not Found"});    // next() next scope without paramater ==> next middleware call pass   /   if parameter in scope ==> Error Handeling call
//     // res.status(404).json({
//     //     result: null,
//     //     status: false,
//     //     message: "Not Found"
//     // });
// });

// This is Error Handeling Middleware
app.use((error, req, res, next) => {

    let status = error.status ?? 500;
    let msg = error.msg ?? error;

    res.status(status).json({
        result: null,
        status: false,
        msg: msg
    })

})
// 
app.listen(3012, "localhost", (err) => {
    if (!err) {
        console.log("Server is listining");
        console.log("Press CTRL+C to disconnect from server...");
        
    };
});      